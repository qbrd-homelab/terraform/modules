# vmware-data

Module with common data bits:

Inputs:

```terraform
variable "datacenter"   { default = "dc1" }
variable "datastore"    { default = "datastore1" }
variable "network"      { default = "VM Network" }
```

Outputs:

```terraform
output "datacenter" { value = data.vsphere_datacenter.dc }
output "datastore"  { value = data.vsphere_datastore.ds }
output "network"    { value = data.vsphere_network.network }
```
