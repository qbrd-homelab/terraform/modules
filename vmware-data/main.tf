variable "datacenter"   { default = "dc1" }
variable "datastore"    { default = "datastore1" }
variable "pool"         { default = "esxi1/Resources"}
variable "network"      { default = "VM Network" }

data "vsphere_datacenter" "dc" { 
    name = var.datacenter
}

data "vsphere_datastore" "ds" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = var.pool
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.network
  datacenter_id = data.vsphere_datacenter.dc.id
}

output "datacenter" { value = data.vsphere_datacenter.dc }
output "datastore"  { value = data.vsphere_datastore.ds }
output "pool"       { value = data.vsphere_resource_pool.pool }
output "network"    { value = data.vsphere_network.network }
